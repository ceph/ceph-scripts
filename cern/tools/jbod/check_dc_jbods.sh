# a wrapper script for check_jbod_cabling.sh collects the result of a run 
# against a set of hosts and aggregates the result for consumption.

project="IT Ceph Ironic"
region="pdc"
output_dir="cdcc_output"
jbod_disks=30 # in pdc, 5 for mds, 10 for local full flash, 30 for jbod spinner

eval $(ai-rc "$project" --region "$region") 
hosts=$(openstack server list -f json | jq -r '.[].Name')

# make output dir clean
rm -rf "$output_dir"
mkdir "$output_dir"

for host in $hosts; do
    ssh_cmd="ssh root@$host -o StrictHostKeyChecking=no" 
    echo "checking $host"
    
    # check if the host has a connected jbod.
    if [[ ! $($ssh_cmd 'lsscsi | wc -l') -ge $jbod_disks ]]; then 
        echo "$host doesn't appear to be connected to a jbod, skipping."
        continue
    fi
    # run the check and pipe the the output into the output dir
    $ssh_cmd 'dnf install smp_utils -y'
    $ssh_cmd '/root/ceph-scripts/cern/tools/jbod/check_jbod_cabling.sh' >> "$output_dir"/$host 
done


echo 'printing results'
sleep 3
for host in $(ls $output_dir); do 
    echo "$host"
    cat "$output_dir"/"$host" | grep -E 'ERROR|good'
done
 
