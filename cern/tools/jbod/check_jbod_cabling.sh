#!/bin/bash

#
# Script to check if JBODs with 60 HDDs zoned in two groups of 30 HDDs each
# are correctly connected to 2 headnodes. See INC4270214.
#
# The basic idea is:
# - Physical identifiers from 0 to 11 correspond to port 1 to 3 on the JBOD
#     --> This is zone 1, i.e., one group of 30 HDDs
# - Physical identifiers from 12 to 23 correspond to port 4 to 6
#     --> This is zone 2, i.e., the second group of 30 HDDs
#
# If there is more than one SAS address for each group, it means that the group
# is wrongly addressed by 2 headnodes, which immediately causes data corruption
# upon block device access.
#
# Not all physical identifiers must be attached: Typically only 4 of each group are used.
#

NUM=$(ls /dev/bsg | grep expander | head -n 1 | awk -F'[-:]' '{print $2}')
CMD="smp_discover /dev/bsg/expander-$NUM:0"
if ! command -v $CMD 2>&1 >/dev/null
then
  echo "ERROR: smp_discover not found"
  exit 1
fi
TMPFILE=$(mktemp)
eval $CMD > $TMPFILE


show() {
  for key in ${!count[@]}; do
    echo "  "${key} ${count[${key}]}
  done
}


count() {
  declare -A count
  local rs=$1
  local re=$2

  while IFS= read -r line; do
    addr=$(echo $line | cut -d ' ' -f 2- | cut -d : -f 1)
    if [[ $addr -ge $rs && $addr -le $re ]];
    then
      attached=$(echo $line | cut -d : -f 3)
      if [ x"$attached" == x"attached" ];
      then
        nodeaddr=$(echo $line | cut -d : -f 4 | cut -c 2-)
        (( count[$nodeaddr]++ ))
      else
        (( count["disabled"]++ ))
      fi
    fi
  done < $TMPFILE

  nds=$( echo ${!count[@]} | wc -w)
  case $nds in
    1)
      echo "ERROR: No nodes connected to zone $rs-$re of the JBOD"
      show "${count[@]}"
      exit 1
      ;;
    2)
      echo "All is good with the JBOD cabling:"
      show "${count[@]}"
      exit 0
      ;;
    3)
      echo "ERROR: Too many nodes attached to zone $rs-$re of the JBOD"
      show "${array[@]}"
      exit 1
      ;;
    *)
      echo "I'm not sure cabling is right... Please check."
      show "${array[@]}"
      exit 1
      ;;
  esac
}

# Turns out one node sees only the zone of the JBOD it is attached to, but this
# can randomly be in either of the 2 physical addresses ranges.
#count 0 11  # First JBOD zone
#count 12 23 # Second JBOD zone

# So we just check both ranges at the same time and hope to see only one SAS address
count 0 23
