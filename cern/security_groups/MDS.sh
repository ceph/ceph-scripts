#!/bin/bash

# Docs at https://clouddocs.web.cern.ch/networking/security_groups.html


unset OS_PROJECT_ID;
unset OS_TENANT_ID;
unset OS_TENANT_NAME;

export OS_PROJECT_NAME="IT Ceph Storage Service";
export OS_REGION_NAME="pdc"


openstack server list  > /dev/null 2>&1
if [ $? != "0" ] ; then
  echo "openstack server list not working"
  exit
fi

GROUP="MDS"
DESCR="Security group for MDS"
PROTO="tcp"
PORTS="6800:7568"
ETHER="IPv4 IPv6"

# echo "Configuring security group for NFSv4..."
# echo "  Group name: $GROUP"
# echo "  Group description: ${DESCR}"
# echo "  L4 protocols: $PROTO"
# echo "  L4 ports: $PORTS"
# echo "  Ether types: $ETHER"

echo openstack security group create --description \'"${DESCR}"\' $GROUP
for proto in $PROTO; do
    for port in $PORTS; do
        for etype in $ETHER; do
            echo openstack security group rule create --ingress --ethertype $etype --protocol $proto --dst-port $port $GROUP
        done
    done
done

echo
echo "# Then, add the security group to VMs with \`openstack server add security group <vm-name> <security-group-name>\`"
