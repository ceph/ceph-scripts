#!/bin/bash

# Docs at https://clouddocs.web.cern.ch/networking/security_groups.html


unset OS_PROJECT_ID;
unset OS_TENANT_ID;
unset OS_TENANT_NAME;

export OS_PROJECT_NAME="IT Ceph Storage Service";
export OS_REGION_NAME="pdc"


openstack server list  > /dev/null 2>&1
if [ $? != "0" ] ; then
  echo "openstack server list not working"
  exit
fi

GROUP="RGW"
DESCR="Security group for Rados Gateways"
ETHER="IPv4 IPv6"

# echo "Configuring security group for NFSv4..."
# echo "  Group name: $GROUP"
# echo "  Group description: ${DESCR}"
# echo "  L4 protocols: $PROTO"
# echo "  L4 ports: $PORTS"
# echo "  Ether types: $ETHER"

echo openstack security group create --description \'"${DESCR}"\' $GROUP
# TCP: HTTP + HTTPS for traefik, 8080 for radosgw backend
for port in 80 443 8080; do
    for etype in $ETHER; do
        echo openstack security group rule create --ingress --ethertype $etype --protocol tcp --dst-port $port $GROUP
    done
done
# UDP: snmp for lbclient
for port in 161; do
    for etype in $ETHER; do
        echo openstack security group rule create --ingress --ethertype $etype --protocol udp --dst-port $port $GROUP
    done
done

echo
echo "# Then, add the security group to VMs with \`openstack server add security group <vm-name> <security-group-name>\`"
