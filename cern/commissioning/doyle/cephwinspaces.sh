#
# This is for repurpusing DFS hardware (cerndataNNN) to Ceph
#   following the decommissioning of the former.
#
# See
#   - CEPH-1624 (https://its.cern.ch/jira/browse/CEPH-1624)
#   - DCRUN32021-3235 (https://its.cern.ch/jira/browse/DCRUN32021-3235)
#
# ** WARNING **
# These machines came with the following fast devices:
#  [1:0:2:0]    disk    ATA      SAMSUNG MZ7L31T9 004Q  /dev/sda
#  [1:0:4:0]    disk    ATA      SAMSUNG MZ7L31T9 004Q  /dev/sdae
#  [1:0:0:0]    disk    ATA      SAMSUNG MZ7L31T9 004Q  /dev/sdd
#  [1:0:6:0]    disk    ATA      SAMSUNG MZ7L31T9 004Q  /dev/sde
#  [1:0:3:0]    disk    ATA      SAMSUNG MZ7LH1T9 904Q  /dev/sdb
#  [1:0:1:0]    disk    ATA      SAMSUNG MZ7LH1T9 904Q  /dev/sdc
#  [1:0:5:0]    disk    ATA      SAMSUNG MZ7LH1T9 904Q  /dev/sdf
#
# ** WARNING **
# Instalation of these nodes requires some manual steps in order to be successful
# please read this .sh file in its entirety before proceeding!

# first, we should bootstrap the ironic node into a useable state:


#!/bin/bash -x
unset OS_PROJECT_ID;
unset OS_TENANT_ID;
unset OS_TENANT_NAME;
export OS_PROJECT_NAME="IT Ceph Ironic";

openstack server list  > /dev/null 2>&1
if [ $? != "0" ] ; then
  echo "openstack server list not working"
  exit
fi

PREFIX='cephwinspaces-'

# after 28/02/2025, with the recabling  we now have three flavours:
# 1x s1.dl8951091.S513-A-IP50
# 1x s1.dl8951091.S513-A-SI558
# 2x s1.dl8951091.S513-A-SI56 
# original flavor was - s1.dl8951091.S513-A-IP56'

# FLAVOR=<FLAVOR>

ai-bs     --landb-mainuser ceph-admins \
          --landb-responsible ceph-admins \
          --nova-flavor $FLAVOR \
          --rhel9 \
          --foreman-environment 'production' \
          --foreman-hostgroup 'ceph/spare' \
          --prefix $PREFIX

# next we need to clean the initial boot drive that ai-bs creates with RHEL9,
# we do this by wiping its partition tables, ensuring that when the node reboots
# after performing ai-install, we don't use the wrong device to startup.
#
#   HOST=<!!!HOSTNAME-GOES-HERE!!!>
#   ssh root@$HOST
#   DEV=$(lsblk -no pkname $(findmnt / | grep dev | awk '{print $2}'))
#   wipefs -a /dev/$DEV --force
#   sfdisk --delete /dev/$DEV --force
#
# finally, we should "reinstall" the node using the correct partition table.
# In this case we want to perform raid-1 (with one spare) using three drives,
# the partition table used bellow hard-codes a grep for MZ7LH1T9HMLT, which is
# the drive model of which we have 3. We also have other 4 drives of type
# MZ7L31T9 that will be used for the metadata pool.
#
# This step should conclude automagically, after roughly half an hour, but keep
# an eye on the IPMI console of the node in case of errors:
#
#   eval $(ai-rc "IT Ceph Ironic")
#   ai-foreman updatehost -e production -p 'Ceph (EFI,SSD+JBOD,winspaces)' --operatingsystem "RHEL 9.3" -m RedHatCERN $HOST
#   ai-installhost --aims-kopts 'inst.sshd' --mode=uefi $HOST
#   openstack server reboot --hard $HOST

