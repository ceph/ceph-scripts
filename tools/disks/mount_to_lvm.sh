#!/bin/bash
# A small script for mapping mounted ceph lvm devices to /dev/<dev> block devices
# and identifiying outliers, unreliant on ceph-metadata or ceph-volume which
# might be incorrect. NOTE only for use on spinning disk clusters, journals are filtered.

path="/var/lib/ceph/osd"
block_devs=$(lsblk | grep disk | grep -v nvme | awk '{print $1}' | xargs)

echo "Script will now corrospond lvm mounts to physical block devices"
sleep 2

for osd in $(ls $path); do
    lvm_dev=$(readlink "$path/$osd/block")
    if [[ -z "$lvm_dev" ]]; then
        echo "no hit for $osd"
        continue
    fi
    # fetch the last portion of the lvm uuid
    result="${lvm_dev##*-}"
    exists=$(lsblk | grep -B 1 "$result" | grep disk | awk '{print $1}')
    echo "$osd | $result | $exists" | tee -a match.txt
done

# look for a negative match, e.g. a drive that has been orphaned but still has lvms.
echo
echo "now printing drives that do not have an lvm mounted"
sleep 2
for dev in $block_devs; do
    if [[ -z $(cat match.txt | grep "$dev") ]]; then
        lsblk "/dev/$dev"
    fi
done

# clean up
rm match.txt
