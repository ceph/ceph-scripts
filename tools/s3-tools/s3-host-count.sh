#!/bin/bash

PROBE=$(hostname -s)
HOSTNAME='s3.cern.ch'
HOSTCOUNT_THRESHOLD=6
TIMEOUT=10
TELEGRAM_SEND='/afs/cern.ch/user/e/ebocchi/.local/bin/telegram-send'
SLACKPOST='/afs/cern.ch/user/e/ebocchi/it-puppet-hostgroup-ceph/code/files/slackpost'


# Make sure you have the tooling to send alert messages
if [ ! -f $TELEGRAM_SEND ]; then
  echo "ERROR: TelegramSend script not found! ($TELEGRAM_SEND) on $PROBE"
  exit 1
fi
if [ ! -f $SLACKPOST ]; then
  echo "ERROR: Slackpost script not found! ($SLACKPOST) on $PROBE"
  exit 1
fi
if ! which host &> /dev/null; then
  $TELEGRAM_SEND "ERROR: /usr/bin/host not found on $PROBE"
  $SLACKPOST 'ceph_bot' "ERROR: /usr/bin/host not found on $PROBE"
  exit 1
fi


timeout $TIMEOUT ping -c 5 -i 0.2 $HOSTNAME > /dev/null 2>&1
if [ $? -ne 0 ]; then
  $TELEGRAM_SEND "Unable to ping $HOSTNAME from $PROBE. Please check!"
  $SLACKPOST 'ceph_ping' "Unable to ping $HOSTNAME from $PROBE. Please check!"
else
  IPv4=$(timeout $TIMEOUT host $HOSTNAME | grep "has address" | wc -l)
  IPv6=$(timeout $TIMEOUT host $HOSTNAME | grep "has IPv6 address" | wc -l)

  if [ $IPv4 -le $HOSTCOUNT_THRESHOLD ]; then
    $TELEGRAM_SEND "$HOSTNAME has only $IPv4 IPv4 addresses (from $PROBE) in the alias!"
    $SLACKPOST 'ceph_hostcount' "$HOSTNAME has only $IPv4 IPv4 addresses (from $PROBE) in the alias!"
  fi
  if [ $IPv6 -le $HOSTCOUNT_THRESHOLD ]; then
    $TELEGRAM_SEND "$HOSTNAME has only $IPv6 IPv6 addresses (from $PROBE) in the alias!"
    $SLACKPOST 'ceph_hostcount' "$HOSTNAME has only $IPv6 IPv6 addresses (from $PROBE) in the alias!"
  fi
fi
