# this script can be ran against traefik on a host to determine differences in
# syntax between releases.

TRAEFIK_VER=$(traefik version | grep Version | awk '{print $2}')
OUTPUT_FILE="access_log_diff_$TRAEFIK_VER"
BUCKET='zgtest'
LOG='/var/log/traefik/access.log'

rm $OUTPUT_FILE; touch $OUTPUT_FILE
rm exampleobj; touch exampleobj; 

tail -f "$LOG" | tee $OUTPUT_FILE &
echo >> 'lorumipsumdelorum' exampleobj

s3cmd mb s3://$BUCKET
s3cmd ls s3://$BUCKET
s3cmd put exampleobj s3://$BUCKET
s3cmd put 'thiswillfail!' s3://$BUCKET
s3cmd ls s3://$BUCKET
s3cmd get s3://$BUCKET/exampleobj
s3cmd del s3://$BUCKET/exampleobj
s3cmd rb s3://$BUCKET

cat "$OUTPUT_FILE" | jq -r 'paths | map(tostring) | join(".")' | sort | uniq  > "$OUTPUT_FILE"_keys

echo "wrote corrosponding logs to "$OUTPUT_FILE""
echo "wrote unique keys to "$OUTPUT_FILE"_keys"
