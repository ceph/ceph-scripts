#!/bin/python3

'''
This script queries OpenSearch to infer usage statistics from S3 access logs
  - It reads a list of access_keys stored in file KEYS_FILE
  - Counts how many times a key has been used
  - Optionally, counts which buckets have been accessed and from with client host

For debugging purposes on your query, consider using the Dev Tools functionality
in OpenSearch and tune your parameters on the need. Example for bucket aggregation hit count:
  POST <index-name>-YYYY.MM.DD/_search
  {
    "size": 0, 
    "query": {
      "wildcard": {
        "request_Authorization": {
          "value": "<whatever-credential-here>"
        }
      }
    },
    "aggs": {
      "buckets": {
        "terms": {
          "field": "bucket_name"
        }
      }
    }, 
    "track_total_hits": true
  }
'''


import socket
from opensearchpy import OpenSearch, RequestsHttpConnection
from requests_kerberos import HTTPKerberosAuth, DISABLED, OPTIONAL

INDEX_PATTERN = "ceph_<index-name>-YYYY.MM.DD"  # Wildcards supported
KEYS_FILE = "keys.txt"

COUNT_BUCKETS = True
COUNT_CLIENTS = True

client = OpenSearch(
    [f'https://os-ceph.cern.ch/os'],
    use_ssl=True,
    verify_certs=True,
    http_compress=True,
    http_auth=HTTPKerberosAuth(mutual_authentication=OPTIONAL),
    ca_certs='/etc/pki/tls/certs/ca-bundle.trust.crt',
    connection_class=RequestsHttpConnection)


def read_keys(file_path):
    with open(file_path, "r") as f:
        return [line.strip() for line in f if line.strip()]


# Query for generic aggregation of terms in buckets
def terms_aggregation(key, field):
    query = {
        "query": {
            "wildcard": {
                "request_Authorization": {
                    "value": f"*{key}*"
                }
             }
         },
        "aggs": {
            "buckets": {
                "terms": {
                    "field": f"{field}"
                }
              }
          },
        "track_total_hits": "true"
    }
    response = client.search(index=INDEX_PATTERN, body=query)
    return response


# Count how many times a key was used
def hit_count(key):
    query = {
        "query": {
            "wildcard": {
                "request_Authorization": {
                    "value": f"*{key}*"
                }
            }
        }
    }
    response = client.count(index=INDEX_PATTERN, body=query)
    return response


if __name__ == "__main__":
    keys = read_keys(KEYS_FILE)

    for key in keys:
        result = hit_count(key).get('count')
        if result:
            print(f"Key: {key}, Count: {result}")

            if COUNT_BUCKETS:
                buckets = terms_aggregation(key, 'bucket_name').get('aggregations')
                for b in buckets['buckets']['buckets']:
                    print(f"\tBucket: {b['key']}, Count: {b['doc_count']}")

            if COUNT_CLIENTS:
                clients = terms_aggregation(key, 'ClientHost').get('aggregations')
                for c in clients['buckets']['buckets']:
                    cip = c['key']
                    try: 
                        ch = socket.gethostbyaddr(cip)[0]
                    except:
                        ch = "unknown host"
                    print(f"\tClientIP: {cip} ({ch}), Count: {c['doc_count']}")
        else:
            print(f"Key: {key}, Count: 0")
