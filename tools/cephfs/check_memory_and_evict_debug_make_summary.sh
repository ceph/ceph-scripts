#!/bin/bash
#
# A additional debug processor for check_memory_and_evict.sh
#
# enrico.bocchi@cern.ch
# cephadmins.cern.ch

folder=check_memory_and_evict_debug
for f in $(ls $folder | grep evicted_client-)
do
    timestamp=$(echo $f | cut -d '-' -f2)
    client_id=$(cat $folder/$f)

    clientls_file=$(echo clientls-$timestamp)
    res=$(cat $folder/$clientls_file | jq -r --arg CID $client_id '.[] | select(.id=='$client_id') | .client_metadata | "\(.entity_id) \(.hostname) \(.root)"')
    when=$(date -Iseconds -d @$timestamp)
    echo $when $res
done
