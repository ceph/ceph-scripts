#!/bin/bash
#
# A memory and release cap based actuator that evicts clients with high release
# caps if memory usage is detected to be above a nominal threshold.
#
# NOTE: this script MUST be profiled agains't your hardware otherwise negligent
# evictions of valid, healthy clients may occur.
# 
# enrico.bocchi@cern.ch
# cephadmins@cern.ch

hostname=$(hostname -s)
debug=0 # Enable the log debug level temporarily
debug_target=20 # target debug level
debug_ms=0 # enable messenger debug
threshold=89071000 # in kb, threshold to trigger the actuator
dump_path="/root/check_memory_and_evict_debug"
mkdir -p $dump_path
now=$(date +%s)

# current memory usage
mds_pid=$(pgrep ceph-mds)
rss_mem_usage=$(cat /proc/${mds_pid}/status | grep VmRSS | awk '{print $2}')

if [[ $rss_mem_usage -gt $threshold ]]; then
  echo "abnormal memory growth: ${rss_mem_usage} kB (threshold: ${threshold} kB)"

  # we get list of clients
  client_ls=$(ceph daemon mds.`hostname -s` client ls)

  # we get the client with more release caps
  cl=$(echo $client_ls | jq -r '.[] | "\(.id), \(.release_caps.value)"' | sort  -k2 -n | tail -1 | cut -d, -f1)

  if [[ $debug -eq 1 ]]; then
      echo "enable debug"
      ceph config set mds.${hostname} debug_mds $debug_target
      if [[ $debug_ms -eq 1 ]]; then
         echo "also enable messenger debug"
         ceph config set mds.${hostname} debug_ms 1
      fi
      sleep 10
  fi

  echo "will evict ${cl}"
  echo $cl >> "$dump_path/evicted_client-$now"
  # evict the client
  ceph tell mds.* client evict id=$cl;
  if [[ $debug -eq 1 ]]; then
     echo "disabling debug"
     ceph config set mds.${hostname} debug_mds 2
     if [[ $debug_ms -eq 1 ]]; then
        echo "also disable messenger debug"
        ceph config set mds.${hostname} debug_ms 0
     fi
  fi
  # save list of clients
  clientls_file="$dump_path/clientls-$now"
  echo $client_ls > $clientls_file
else
    echo "memory ok: ${rss_mem_usage} kB"
fi
