#!/bin/bash
#
# A release cap based actuator for evicting clients that may cause MDS spinlock
#
# enrico.bocchi@cern.ch
# cephadmins@cern.ch

hostname=$(hostname -s)
debug=0 # Enable the log debug level temporarily
debug_target=20 # target debug level
debug_ms=0 # enable messenger debug
threshold=1000000 # value of release_caps, threshold to trigger the actuator
dump_path="/root/check_releasecaps_and_evict_debug"
mkdir -p $dump_path
now=$(date +%s)

# current release_caps counter

# we get list of clients
client_ls=$(ceph daemon mds.`hostname -s` client ls)

# we get the client with more release caps
top_client=$(echo $client_ls | jq -r '.[] | "\(.id), \(.release_caps.value)"' | sort -k2 -n | tail -1)
cl=$(echo $top_client| cut -d, -f 1)
caps=$(echo $top_client | cut -d, -f 2)
caps_int=$(echo $caps | awk '{print int($0)}')

if [[ $caps_int -gt $threshold ]]; then
  echo "client $cl has too many release caps! ($caps)"
  echo "will evict $cl"

  # evict the client
  ## echo "client $cl -- not evicting for now"
  ceph tell mds.* client evict id=$cl;

  # save list of clients
  echo $cl >> "$dump_path/evicted_client-$now"
  echo $client_ls >> "$dump_path/clientls-$now"
else
  echo "OK -- No release_caps runaway. Highest is $cl with $caps caps"
fi
