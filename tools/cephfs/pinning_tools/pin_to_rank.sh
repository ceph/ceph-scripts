#!/bin/bash

FS_PATHS_FILE="$1"
LOCAL_MOUNTPOINT="/cephfs-flax"

DESIRED_PIN=3
MAX_ENTRIES=100000
SLEEP_TIME=0

show_usage()
{
  echo "usage:"
  echo "./pin_to_rank.sh <fs-paths-file>"
  echo "    fs-paths-file:         File containing list of fs_paths (produced by get_project_shares.sh)"
}

if [ x"$FS_PATHS_FILE" == x"" ];
then
  echo
  echo "ERROR: Input file not provided."
  show_usage
  exit 1
fi

while IFS= read -r line
do
  share_path=$(echo $line | cut -d  ' ' -f 2)
  share_root=$(echo $share_path | cut -d '/' -f 4)
  fs_path=$LOCAL_MOUNTPOINT"/volumes/_nogroup/"$share_root

  stat $fs_path > /dev/null 2>&1
  if [ $? -ne 0 ];
  then
    echo "  # $fs_path - cannot stat"
    continue
  fi

  pin=$(getfattr --absolute-names -n ceph.dir.pin $fs_path | grep ^ceph.dir.pin | cut -d '"' -f 2)
  if [ $pin -ne $DESIRED_PIN ];
  then
    entries=$(getfattr --absolute-names -n ceph.dir.rentries $fs_path | grep ^ceph.dir.rentries | cut -d '"' -f 2)
    if [ $entries -lt $MAX_ENTRIES ];
    then
      echo "  # $fs_path - pin: $pin, desired: $DESIRED_PIN, entries: $entries (OK: pinning...)"
      echo "PINNING_CMD: setfattr -n ceph.dir.pin -v $DESIRED_PIN $fs_path"
      sleep $SLEEP_TIME
    else
      echo "  # $fs_path - pin: $pin, desired: $DESIRED_PIN, entries: $entries (NOT_OK: too many entries...)"
      echo "$FS_PATHS_FILE $fs_path $entries" >> too_many_entries.log
    fi
  else
    echo "  # $fs_path - pin: $pin, desired: $DESIRED_PIN (OK: Already on desired rank)"
  fi
done < $FS_PATHS_FILE
