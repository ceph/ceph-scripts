#!/bin/bash

OS_PROJECT_NAME=services
OS_REGION_NAME=cern

PRJ_ID="$1"
PRJ_NAME="$2"

show_usage()
{
  echo "usage:"
  echo "./get_project_shares.sh <project-id> <human-readble-name>"
  echo "    project-id:         OpenStack project ID"
  echo "    human-redable-name: Annotation to easily idenity result folder/files"
}

if [ x"$PRJ_ID" == x"" ];
then
  echo
  echo "ERROR: Project ID not provided."
  show_usage
  exit 1
fi


# Write one json file per share in subfolder names as project-id
# or project-id + human-readable-name, if provided
OUTPUT_FOLDER=$PRJ_ID
if [ x"$PRJ_NAME" != x"" ];
then
  PRJ_NAME=$(echo $PRJ_NAME | tr ' ' '_')
  OUTPUT_FOLDER=$PRJ_ID"_"$PRJ_NAME
fi
mkdir -p $OUTPUT_FOLDER


# Also write an output file summarizing share uuid and share path on the fs
OUTPUT_FILE=$OUTPUT_FOLDER".fs_paths"
touch $OUTPUT_FILE


# Do the job...
SHARE_LIST=$(openstack share list --project $PRJ_ID -f json)
SHARE_IDs=$(echo $SHARE_LIST | jq -r '.[] | select (.Host=="manila@flax#cephfs") | .ID')
SHARE_NO=$(echo $SHARE_IDs | wc -w)

echo "  # Project $PRJ_ID"
echo "  # Number of shares: $SHARE_NO"
echo "  # Output at: $OUTPUT_FOLDER"
echo "  # FS paths at: $OUTPUT_FILE"

count=1
for share in $SHARE_IDs
do
  echo "    # $count, $share"
  SHARE_INFO=$(openstack share show $share -f json)
  echo $SHARE_INFO > $OUTPUT_FOLDER/$share.json
  fs_path='/'$(echo $SHARE_INFO | jq -r .export_locations | grep ^path | cut -d ' ' -f 3 | cut -d '/' -f 2-)
  echo "$share $fs_path" >> $OUTPUT_FILE
  count=$((count+1))
done
