#!/bin/sh


usage() {
  echo "usage: ./ceph_kernel_debug.sh <mode>"
  echo "  <mode>: --enable to enable kernel debugging"
  echo "          --disable to disable kernel debugging"
}

on() {
  echo "$* +p" > /sys/kernel/debug/dynamic_debug/control
}

off() {
  echo "$* -p" > /sys/kernel/debug/dynamic_debug/control
}

#echo 9 > /proc/sysrq-trigger

mode=$1
if [ x"$mode" == x"" ];
then
  echo "Error: mode not specified."
  usage
  exit
fi

case $mode in
  --enable)
    echo "Enabling kernel client troubleshooting..."
    on 'module ceph'
    on 'module libceph'
    on 'module rbd'
    ;;
  --disable)
    echo "Disabling kernel client troubleshooting..."
    off 'module ceph'    
    off 'module libceph'
    off 'module rbd'
    ;;
  *)
    echo "Error: Unsupported option"
    usage
    ;;
esac
